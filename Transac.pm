package Transac;
# Jan 2023
# Transactions test exercise by mac-t@yandex.ru
# Used 4 chars tab size

use strict;
use utf8;
use base qw(Class::Accessor);

use vars qw($AUTOLOAD);
our $AUTOLOAD;
our $VERSION = '0.0.1';

my @steps = qw /on_backup on_execute on_reject on_commit/;

__PACKAGE__->mk_ro_accessors( qw /result error message stack/ );

#############
sub new {	#
#############
	my $class = shift;
	my $init = { @_ };
	my $self = bless( $init, $class);
	$self->{'stack'} = [];
	return $self;
}
#############
sub create {	#		Append transaction operation
#############
	my $self = shift;
	my $init = {@_};

	my $step = {};
	for my $opname ( @steps) {
		$step->{$opname} = $self->{$opname} if $self->{$opname};
		$step->{$opname} = $init->{$opname} if $init->{$opname};
	}

	$self->{'error'} = 0;
	$self->{'message'} = 'OK';
	undef $self->{'result'};

	if ( ref($step->{'on_backup'}->[0]) eq 'CODE' ) {
		eval { $step->{'on_backup'}->[0]->($step->{'on_backup'}->[1]) };
		if ( $@ ) {
			$self->{'error'} = 10;
			$self->{'message'} = $self->length." BACKUP: $@";
		}
	} elsif( ref($step->{'on_reject'}->[0]) ne 'CODE' ) {
		$self->{'error'} = 42;
		$self->{'message'} = $self->length." REJECT: Not code ref";
	}

	unless ( $self->{'error'} ) {
		if ( ref($step->{'on_execute'}->[0]) eq 'CODE' ) {
			eval { $self->{'result'} = $step->{'on_execute'}->[0]->($step->{'on_execute'}->[1]) };
			if ( $@ ) {
				$self->{'error'} = 20;
				$self->{'message'} = $self->length." EXECUTE: $@";
			}
		} else {
			$self->{'error'} = 22;
			$self->{'message'} = $self->length." EXECUTE: Not code ref";
		}
	}

	return $self if $self->{'error'};
	push( @{$self->{'stack'}}, $step);

	return $self;
}
#################
sub length {	#	Return stack length
#################
	my $self = shift;
	return scalar( @{$self->{'stack'}});
}
#################
sub reject {	#		Try to restore state
#################
	my $self = shift;
	my $nstep = shift || 1;

	if ( $nstep > $self->length ) {
		$self->{'error'} = 41;
		$self->{'message'} = "REJECT: $nstep > ".$self->length;
		return undef;
	}

	undef $self->{'result'};
	while ( $nstep-- ) {
		my $step = $self->{'stack'}->[-1];

		if ( ref($step->{'on_reject'}->[0]) eq 'CODE' ) {
			eval { $step->{'on_reject'}->[0]->($step->{'on_reject'}->[1]) };
			if ( $@ ) {
				$self->{'error'} = 40;
				$self->{'message'} = $self->length." REJECT: $@";
				return undef;
			}
			pop( @{$self->{'stack'}});
		}			# Else do nothing!
	}
	return $self->length;
}
#################
sub commit {	#
#################
	my $self = shift;
	undef $self->{'result'};

	my $nstep = 0;
	while( my $step = $self->{'stack'}->[0] ) {

		$nstep++;
		if ( exists( $step->{'on_commit'}) ) {
			if ( ref($step->{'on_commit'}->[0]) eq 'CODE' ) {
				eval { $step->{'on_commit'}->[0]->($step->{'on_commit'}->[1]) };
				if ( $@ ) {
					$self->{'error'} = 30;
					$self->{'message'} = "$nstep of ".$self->length." COMMIT: $@";
					return undef;
				}
			} else {
				$self->{'error'} = 32;
				$self->{'message'} = "$nstep of ".$self->length." COMMIT: Not code ref";
				return undef;
			}
			shift( @{$self->{'stack'}} );
		} else {
			shift( @{$self->{'stack'}} );
		}

	}
	return $nstep;
}
#############################
sub AUTOLOAD {		# 	Shortcut for on_backup, on_etc. Instead accessors
#############################
	my $self= shift;
	my $method = $AUTOLOAD;		# Unknown What?!;
	$method =~ s/^.*:://;		# 
	$method = lc($method);
	if ( grep { $_ eq $method } @steps) {
		$self->{$method} = \@_;
		return $self;
	}
	die "Unknown transaction method $method";
}
#################
sub DESTROY {	#		Finally
#################
	my $self = shift;
	return undef $self;
}
1
