package Opervs;
# Jan 2023
# Atomic operations test exercise by mac-t@yandex.ru
# Used 4 chars tab size

use strict;
use utf8;
use Time::HiRes;
use Sys::Hostname;
use Cache::Memcached::Fast;
use base qw(Class::Accessor);

our $VERSION = '0.0.1';
__PACKAGE__->mk_accessors( qw /servers memcache max_stack message error lock_expire lock_try/ );
__PACKAGE__->mk_ro_accessors( qw /name changes/ );

#############
sub new {	#
#############
	my $class = shift;
	my $init = { @_ };
	my $self = bless( $init, $class);

	$self->{'name'} = "op::".hostname()."_$$" unless $self->{'name'};
	$self->lock_try(3) unless $self->{'lock_try'};
	$self->lock_expire(3) unless $self->{'lock_expire'};
	$self->max_stack(10) unless $self->{'max_stack'};
	$self->servers( ['localhost:11211']) unless $self->{'servers'};

	$self->memcache( Cache::Memcached::Fast->new( {
							servers => $self->servers,
							namespace => $self->{'name'},
							hash_namespace => 1,
							max_size => $self->{'max_size'} || 1024*1024,
							max_failures => $self->lock_try,
							failure_timeout => $self->lock_expire,
							connect_timeout => 0.2,
							io_timeout => 0.5,
							close_on_error => 1,
							compress_threshold => 100_000,
							compress_ratio => 0.8,
# 							compress_methods => [ \&IO::Compress::Gzip::gzip,
# 												\&IO::Uncompress::Gunzip::gunzip ],
						})
					);
	return undef unless $self->memcache;

	$self->{'lock_key'} = "__LOCK__";
	$self->{'stack_keys'} = [];
	for (0..$self->{'max_stack'}-1 ) {		# Indexing available stack
		push( @{$self->{'stack_keys'}}, $_);
	}

	$self->get_stack;		# Preflight for job
	return $self;
}
#############
sub pop {	#		Pop NN items from tail of stack. LIFO
#############
	my $self = shift;
	my $nstep = shift || 1;
	my $slice = [];
	$self->error(0);
	$self->message("OK");

	unless ( $self->_lock ) {			# Lock before read stack
		$self->error(1);
		$self->message("POP - lock error ($self->{'locked_by'}->{'owner'})");
		return $slice;
	}

	my $stack = $self->get_stack;

	unless ( scalar( @$stack) ) {
		$self->_unlock;
		return $slice ;
	}

	my $latest = $#$stack;
	my $earliest = $latest - $nstep +1;
	$earliest = $latest if $earliest < 0;		# Some user mistakes?

	my $to_delete = [];
	for ($earliest..$latest) {
		unshift( @$to_delete, $_);
	}

	for my $key ( @$to_delete) {		# Delete from latest
		my $res = $self->memcache->delete( $key);
		if ( $res ) {
			unshift( @$slice, pop( @$stack));
			$self->{'changes'}->[1] --;

		} elsif( defined $res ) {
			$self->error(30);
			$self->message("POP - refused by server");
		} else {
			$self->error(31);
			$self->message("POP - miscellaneous error");
		}
		last if $self->error;
	}
	$self->_unlock;

	return $slice;
}
#################
sub transact {	#		Execute an action and save result
#################
	my ($self, $action, $action_params) = @_;
	my $ret;
	my $result = {};

	eval { 
			$result->{'out'} = $action->( $action_params);
			$result->{'in'} = $action_params;		# Params may be reassigned in action
		};
	if ( $@ ) {
		$self->error(20);
		$self->message("EXEC - $@");
		return $ret;
	}

	if ( $self->_lock ) {
		my $stack = $self->get_stack();		#
		if ( $self->_assign( scalar( @$stack), $result) ) {
			$ret = $result;
			$self->{'changes'}->[1]++;
		}
		$self->_unlock;
	} else {
		$self->error(1);
		$self->message("EXEC - lock error ($self->{'locked_by'}->{'owner'})");
	}
	return $ret;
}

#############
sub add {	#		Append next_key to cache, if possible
#############
	my ($self, $data) = @_;
	my $success;

	unless ( $self->_lock ) {			# Lock before get actual stack
		$self->error(1);
		$self->message("ADD - lock error ($self->{'locked_by'}->{'owner'})");
		return $success;
	}

	my $stack = $self->get_stack();

	if ( scalar( @$stack) < $self->{'max_stack'} ) {		# Can we add something?
		$success = $self->_assign( scalar( @$stack), $data );
		$self->{'changes'}->[1] ++ unless $self->error;

	} else {
		$self->error(-1);
		$self->message("Stack oveflow ($self->{'max_stack'})");
	}
	$self->_unlock;

	return $success;
}
#################
sub get_stack {	#		Read stack as ARRAYref and obtain next available Memcache key
#################
	my $self = shift;
	my $stack = [];
	my $stored = $self->memcache->get_multi( @{$self->{'stack_keys'}});

	for my $next_key ( @{$self->{'stack_keys'}}) {
		if ( exists( $stored->{$next_key}) ) {
			push( @$stack, $stored->{$next_key});
		} else {
			last;
		}
	}
	$self->{'changes'} = [scalar( @$stack), scalar( @$stack)];
	return $stack;
}
#############
sub _assign {	#		Assign passed key WITHOUT locking stack
#############
	my ($self, $key, $data) = @_;

	my $success = $self->memcache->add( $key, $data, $self->{'expire_time'});
	if ( $success ) {
		$self->error(0);
		$self->message("OK");

	} elsif( defined $success) {
		$self->error(10);
		$self->message("ADD - refused by server");

	} else {
		$self->error(11);
		$self->message("ADD - miscellaneous error");
	}
	return $success;
}
#############
sub _unlock {	#		Remove 'LOCKED' flag
#############
	my $self = shift;
	my $unlock;
	my $lock = $self->memcache->get($self->{'lock_key'});
	$unlock = ($self->memcache->delete($self->{'lock_key'}) > 0) if $lock && $lock->{'owner'} == $$;
	return $unlock;
}
#############
sub _lock {	#		Try to set 'LOCKED' flag
#############
	my $self = shift;
	my $try = $self->{'lock_try'};
	my $locked;

	my $now = Time::HiRes::time();

	while ( $try-- ) {
		$locked = $self->memcache->add( $self->{'lock_key'}, 
										{'owner' => $$, 'time' => Time::HiRes::time() }, 
										$self->{'lock_expire'}
									);
		last if $locked;
		$self->{'locked_by'} = $self->memcache->get( $self->{'lock_key'});			# To report error
		my $time_est =  $self->{'locked_by'}->{'time'} + $self->{'lock_expire'} - $now;
		sleep( $time_est) if $time_est > 0;
	}

	return $locked;
}
#################
sub DESTROY {	#		Finally
#################
	my $self = shift;
	return undef $self;
}
1
