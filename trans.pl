#!/usr/bin/env perl

use strict;
use utf8;
use 5.18.0;
use Data::Dumper;
use Transac;
use Opervs;

#### Prepare some
my $memcs = Opervs->new(		# https://gitlab.com/kvadik19/operus
				name => 'Trans::',
				max_stack => 5,
				lock_expire => 2,
				lock_try => 2,
				expire_time => 1*60*60*24,
			);
$memcs->pop( scalar( @{$memcs->get_stack} ) );		# Cleanup memcache stack
$memcs->add( {'init' => ['initial', 'memcache', 'content']} );
####

my $data = [10, 20, 30, 40];		# Initial data list
print "Init data: ", Dumper($data);

######### 1. Increment values

my $tr = Transac->new()
				->on_backup( sub{ my $params = shift; return $params}, $data)
				->on_execute( sub{ my $params = shift; do { $_++ } for @$params; return $params}, $data)
				->on_commit( sub{ my $params = shift; return 1}, $data)
				->on_reject( sub{ my $params = shift; do { $_-- } for @$params; return $params}, $data)
				->create;

say "Can't create transaction: ", $tr->message if $tr->error;
my $res1 = $tr->result;


######### 2. Add something into memcache

my @pre_stack = ();
my $add_ok = $tr->create( 
						on_backup => [ sub{ push( @pre_stack, $memcs->get_stack) } ],
						on_execute => [ sub{ my $params = shift; return $memcs->add( $params )}, $data],
						on_reject => [ sub{	my $bkup = pop( @pre_stack);
											$memcs->pop( scalar( @{$memcs->get_stack} ) );
											do { $memcs->add( $_ ) } for @$bkup;
										}]
					)->result;
say "Transaction aborted: ", $tr->message if $tr->error;
$tr->reject(1) unless $add_ok;


######### 3. Divide values

my $res2 = $tr->create( 
						on_execute => [ sub{ my $params = shift; do { $_/=2 } for (@$params); return $params}, $data],
						on_reject => [ sub{ my $params = shift; do { $_*=2 } for (@$params); return $params}, $data]
					)->result;
say "Can't create transaction: ", $tr->message if $tr->error;


######### 4. Add something into memcache again

$add_ok = $tr->create( 
						on_backup => [ sub{ push( @pre_stack, $memcs->get_stack) } ],
						on_execute => [ sub{ my $params = shift; return $memcs->add( $params )}, $data],
						on_reject => [ sub{	my $bkup = pop( @pre_stack);
											$memcs->pop( scalar( @{$memcs->get_stack} ) );
											do { $memcs->add( $_ ) } for @$bkup;
										}]
					)->result;
say "Transaction aborted: ", $tr->message if $tr->error;
$tr->reject(1) unless $add_ok;
print "Opervs memcache result: ", Dumper( $memcs->get_stack );


######### 5. Whole OS operation

my $res3 = $tr->on_backup( sub{ my $params = shift; say "dd -if=/dev/sda0 -of=/var/0.dimg"; return $params}, $res2)
			->on_execute( sub{ my $params = shift; say "mkfs -t ext$params->[0] /dev/sda0"; return $params}, $res2)
			->on_commit( sub{ say "All Your partitions are successfully updated now!" })
			->on_reject( sub{ my $params = shift;  say "dd -if=/var/0.dimg -of=/dev/sda0"; return $params}, $res2)
			->create->result;
say $tr->message if $tr->error;


######### reject / commit

say "Rejected 2 items. Left to commit: ", $tr->reject(2);		# Affected steps 5, 4
say $tr->message if $tr->error;

print "Opervs memcache rejected: ", Dumper( $memcs->get_stack );
say "Comitted success: ", $tr->commit;
print "Resulted data: ", Dumper($data);
